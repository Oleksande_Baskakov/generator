
function worker(gen, ...args) {
  let myRes = null;
  const iterator = gen(...args);
  const firstStep = iterator.next();

  function recurse(step) {
    if (step.done) {
      return myRes(step.value);
    } else {
      if (typeof step.value === 'function') {
        try {
          const newStep = iterator.next(step.value());

          return step.done ? newStep : recurse(newStep);
        } catch (e) {
          recurse(iterator.throw(step.value));
        }
      }

      if (step.value instanceof Promise) {
        step.value.then(res => {
          const newStep = iterator.next(res);

          return step.done ? newStep : recurse(newStep);
        });
      }

      if (typeof step.value === 'number') {
        const newStep = iterator.next(Promise.resolve(step.value));

        return step.done ? newStep : recurse(newStep);
      }
    }
  }
  recurse(firstStep);
  return new Promise(res => {
    myRes = res;
  });
}

