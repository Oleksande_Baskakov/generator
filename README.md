# own-promise
 >Hello everybody!!!

Here is my function worker! It works with generators and can do a lot of things. Function can deal with other functions, promises and numbers.
  
***
Example of use:
```
const sum = (a, b) => {
  if (!a || !b) {
    throw new TypeError();
  }
  return a + b;
};

function* z(a, b) {
  console.log('start');

  let sumRes;

  try {
    sumRes = yield () => sum(a, b); // 6
    console.log(`res 1: ${sumRes}`);
  } catch {
    console.log('res 1: ==> error');
  }

  let promiseRes = yield Promise.resolve(10); // 10
  console.log(`res 2: ${promiseRes}`);

  sumRes = yield () => sum(promiseRes, promiseRes); // 20
  console.log(`res 3: ${sumRes}`);

  promiseRes = yield Promise.resolve(sumRes); // 20
  console.log(`res 4: ${promiseRes}`);

  res = yield 3 + 3; // 6

  console.log('finish');
  return res;
}

worker2(z, 3, 3).then(console.log);
```
***
Good luck and have a nice day!
